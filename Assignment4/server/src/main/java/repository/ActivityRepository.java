package repository;

import interfacing.sensor.Activity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ActivityRepository extends CrudRepository<Activity, Long> {
    List<Activity> findActivitiesByPatientID(Long id);
}
