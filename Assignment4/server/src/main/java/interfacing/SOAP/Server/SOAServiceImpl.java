package interfacing.SOAP.Server;

import interfacing.pillDispenser.Message;
import interfacing.sensor.Activity;
import model.Patient;
import model.Recommendation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import repository.ActivityRepository;
import repository.MessageRepository;
import repository.PatientRepository;
import repository.RecommendationRepository;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@Service
@WebService(endpointInterface = "interfacing.SOAP.Server.SOAService")
public class SOAServiceImpl implements SOAService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private RecommendationRepository recommendationRepository;

    @WebMethod
    public List<Patient> getPatients(){
        return (List<Patient>) patientRepository.findAll();
    }

    @WebMethod
    public List<Activity> getActivityByPatientId(Long id){
        return activityRepository.findActivitiesByPatientID(id);
    }

    @WebMethod
    public List<Message> getMessageByPatientId(Long id){
        return messageRepository.findMessageByPatientID(id);
    }

    @WebMethod
    public void saveRecommendation(Recommendation recommendation){
        recommendationRepository.save(recommendation);
    }
}
