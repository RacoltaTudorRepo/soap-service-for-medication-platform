package interfacing.SOAP.Server;

import model.Recommendation;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface SOAServiceCaregiver {
    @WebMethod
    public List<Recommendation> getRecommendations();

}
