package interfacing.SOAP.Server;

import model.Recommendation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.RecommendationRepository;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@Service
@WebService(endpointInterface = "interfacing.SOAP.Server.SOAServiceCaregiver")
public class SOAServiceCaregiverImpl implements SOAServiceCaregiver{

    @Autowired
    private RecommendationRepository recommendationRepository;

    @WebMethod
    public List<Recommendation> getRecommendations() {
        return (List<Recommendation>)recommendationRepository.findAll();
    }
}
