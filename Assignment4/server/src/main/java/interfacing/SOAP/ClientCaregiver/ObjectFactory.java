
package interfacing.SOAP.ClientCaregiver;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the interfacing.SOAP.ClientCaregiver package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetRecommendations_QNAME = new QName("http://Server.SOAP.interfacing/", "getRecommendations");
    private final static QName _GetRecommendationsResponse_QNAME = new QName("http://Server.SOAP.interfacing/", "getRecommendationsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: interfacing.SOAP.ClientCaregiver
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetRecommendationsResponse }
     * 
     */
    public GetRecommendationsResponse createGetRecommendationsResponse() {
        return new GetRecommendationsResponse();
    }

    /**
     * Create an instance of {@link GetRecommendations }
     * 
     */
    public GetRecommendations createGetRecommendations() {
        return new GetRecommendations();
    }

    /**
     * Create an instance of {@link Recommendation }
     * 
     */
    public Recommendation createRecommendation() {
        return new Recommendation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRecommendations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Server.SOAP.interfacing/", name = "getRecommendations")
    public JAXBElement<GetRecommendations> createGetRecommendations(GetRecommendations value) {
        return new JAXBElement<GetRecommendations>(_GetRecommendations_QNAME, GetRecommendations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRecommendationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://Server.SOAP.interfacing/", name = "getRecommendationsResponse")
    public JAXBElement<GetRecommendationsResponse> createGetRecommendationsResponse(GetRecommendationsResponse value) {
        return new JAXBElement<GetRecommendationsResponse>(_GetRecommendationsResponse_QNAME, GetRecommendationsResponse.class, null, value);
    }

}
