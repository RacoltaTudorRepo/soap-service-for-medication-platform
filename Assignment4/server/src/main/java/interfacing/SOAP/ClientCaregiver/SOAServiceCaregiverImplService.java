
package interfacing.SOAP.ClientCaregiver;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SOAServiceCaregiverImplService", targetNamespace = "http://Server.SOAP.interfacing/", wsdlLocation = "http://localhost:8999/soaserviceCaregiver?wsdl")
public class SOAServiceCaregiverImplService
    extends Service
{

    private final static URL SOASERVICECAREGIVERIMPLSERVICE_WSDL_LOCATION;
    private final static WebServiceException SOASERVICECAREGIVERIMPLSERVICE_EXCEPTION;
    private final static QName SOASERVICECAREGIVERIMPLSERVICE_QNAME = new QName("http://Server.SOAP.interfacing/", "SOAServiceCaregiverImplService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8999/soaserviceCaregiver?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SOASERVICECAREGIVERIMPLSERVICE_WSDL_LOCATION = url;
        SOASERVICECAREGIVERIMPLSERVICE_EXCEPTION = e;
    }

    public SOAServiceCaregiverImplService() {
        super(__getWsdlLocation(), SOASERVICECAREGIVERIMPLSERVICE_QNAME);
    }

    public SOAServiceCaregiverImplService(WebServiceFeature... features) {
        super(__getWsdlLocation(), SOASERVICECAREGIVERIMPLSERVICE_QNAME, features);
    }

    public SOAServiceCaregiverImplService(URL wsdlLocation) {
        super(wsdlLocation, SOASERVICECAREGIVERIMPLSERVICE_QNAME);
    }

    public SOAServiceCaregiverImplService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SOASERVICECAREGIVERIMPLSERVICE_QNAME, features);
    }

    public SOAServiceCaregiverImplService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SOAServiceCaregiverImplService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns SOAServiceCaregiver
     */
    @WebEndpoint(name = "SOAServiceCaregiverImplPort")
    public SOAServiceCaregiver getSOAServiceCaregiverImplPort() {
        return super.getPort(new QName("http://Server.SOAP.interfacing/", "SOAServiceCaregiverImplPort"), SOAServiceCaregiver.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SOAServiceCaregiver
     */
    @WebEndpoint(name = "SOAServiceCaregiverImplPort")
    public SOAServiceCaregiver getSOAServiceCaregiverImplPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://Server.SOAP.interfacing/", "SOAServiceCaregiverImplPort"), SOAServiceCaregiver.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SOASERVICECAREGIVERIMPLSERVICE_EXCEPTION!= null) {
            throw SOASERVICECAREGIVERIMPLSERVICE_EXCEPTION;
        }
        return SOASERVICECAREGIVERIMPLSERVICE_WSDL_LOCATION;
    }

}
