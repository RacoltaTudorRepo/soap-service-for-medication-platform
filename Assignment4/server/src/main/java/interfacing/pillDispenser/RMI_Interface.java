package interfacing.pillDispenser;

import model.Medication;
import model.MedicationPlan;
import model.Patient;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface RMI_Interface{
    List<MedicationPlan> getMedicationPlans(Long patientID);
    Patient loginPatient(String username, String password);
    void saveMessage(Medication medication,boolean taken, String reason,Long patientID);
}
