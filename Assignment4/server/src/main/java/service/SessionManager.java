package service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class SessionManager {
    private ArrayList<String> users;

    public SessionManager(){
        users=new ArrayList<>();
    }

    public void addUser(String username){
        users.add(username);
    }

    public void removeUser(String username){
        users.remove(username);
    }

    public boolean isLoggedIn(String username){
        if(users.contains(username))
            return true;
        return false;
    }
}
