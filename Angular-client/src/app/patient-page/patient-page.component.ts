import {AfterViewInit, Component, OnInit} from '@angular/core';
import {MedicationPlan} from "../model/MedicationPlan";
import {MedicationService} from "../service/medication.service";
import {Patient} from "../model/Patient";
import {AuthGuardService} from "../service/auth-guard.service";
import {HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-patient-page',
  templateUrl: './patient-page.component.html',
  styleUrls: ['./patient-page.component.css']
})
export class PatientPageComponent implements OnInit,AfterViewInit {
  patient:Patient;
  medicationPlans:MedicationPlan[];
  headers:HttpHeaders;

  constructor(private medicationService:MedicationService) {
    this.headers= new HttpHeaders();
    this.headers=this.headers.append("medication","true");
  }

  ngOnInit() {
    this.patient=JSON.parse(localStorage.getItem("user"));
    this.medicationService.getMedicationPlansForPatient(this.patient.id,this.headers).subscribe(data=>{
      this.medicationPlans=data;
      console.log(this.medicationPlans)
    })
  }

  ngAfterViewInit(): void {

  }


}
