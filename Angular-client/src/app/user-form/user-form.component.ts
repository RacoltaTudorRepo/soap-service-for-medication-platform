import {Component, Input} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../service/user-service';
import { User } from '../model/user';
import {Caregiver} from "../model/Caregiver";
import {Patient} from "../model/Patient";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent {

  @Input() role:string;
  @Input() title:string;
  @Input() header:string;
  @Input() patient:Patient;
  @Input() caregiver:Caregiver;
  @Input() allPatients:Patient[];

  constructor(private router: Router, private userService: UserService) {
  }

  confirmUser() {
    if(this.role==='Patient') {
      this.patient.userROLE = this.role;
      //this.userService.modifyPatient(this.patient).subscribe();
    }
    else{
      this.caregiver.userROLE=this.role;
      //this.userService.modifyCaregiver(this.caregiver).subscribe();
    }

  }

  addPatient(patient:Patient){
    this.caregiver.patients.push(patient);
    console.log(this.caregiver.patients);
  }

  clearPatients(){
    this.caregiver.patients=[];
  }
}