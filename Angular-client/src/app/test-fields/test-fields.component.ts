import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-test-fields',
  templateUrl: './test-fields.component.html',
  styleUrls: ['./test-fields.component.css']
})
export class TestFieldsComponent implements OnInit {
  @Input()role:string;

  constructor() { }

  ngOnInit() {
  }

}
