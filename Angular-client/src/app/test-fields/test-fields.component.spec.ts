import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestFieldsComponent } from './test-fields.component';

describe('TestFieldsComponent', () => {
  let component: TestFieldsComponent;
  let fixture: ComponentFixture<TestFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
