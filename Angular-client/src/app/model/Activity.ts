import DateTimeFormat = Intl.DateTimeFormat;

export class Activity{
    activityId:number;
    name:string;
    patientID:number;
    startTime:string;
    endTime:string;
    valid:boolean;
}