export class Message{
    id:number;
    medicationName:string;
    taken:boolean;
    reason:string;
    patientID:number;
    date:string;
}